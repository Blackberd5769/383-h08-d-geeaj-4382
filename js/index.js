//author: austin gee 
//uniqueID: geeaj 
//class: cse383
//assignment: homework07
//file: js file.

//NOTE: if I show the alert there is a problem and we return false 
//so it stops the check ther for the submitdata function

//always running and hides the alerts so it doesnt clutter the screen
$(document).ready(function() {
    $('.alert').hide();
    $("#table1").hide();
} );

//test for the user input
function Validate1(){
  var x = document.getElementById("usr").value;

  // if more than 3 char
  if (x.trim().length < 3) {   
        $('#verifier1').show();
        return false;
  } else {
        $('#verifier1').hide();
        return true;
  }
}

function Validate2(){
    var x = document.getElementById("affiliation").value;

    //looks at the value of affiliation if empty is the value user hasnt picked an element yet
    if (x == "empty") 
    {
        $('#verifier2').show();
        return false;
   } else {
        $('#verifier2').hide();
        return true;
   }
}

function Validate3(){
    var x = document.getElementById("comment").value;

    //counts the white spaces
    var spaces = (x.split(" ").length - 1); 
    if ((!x) ||  (spaces < 2) || (x.includes("<") || (x.includes(">"))) || !(x.trim().length > 0)){
        $('#verifier3').show();
    } else {
        $('#verifier3').hide();
        return true;
    }
}

function Validate4(){
    var x = document.getElementById("car").value;

    //checks to see if there is not whitespace and more than 1 char
    if (!(x.trim().length > 0)){ 
        $('#verifier4').show();
    } else {
        $('#verifier4').hide();
        return true;
    }
}

function Validate5(){
    var x = document.getElementById("color").value;

    //looks at the value of affiliation if empty is the value user hasnt picked an element yet
    if (x == "empty")
    {
        $('#verifier5').show();
        return false;
   } else {
        $('#verifier5').hide();
        return true;
   }
}

function SubmitData(){

    //this if test makes sure that only one validation is done at a time by 
    //returning nothing so it exits the function if the validation functions return false;
    if (Validate1()){} else {
        $('#verifier1').show();
        return false;
    }
    if (Validate2()){} else {
        $('#verifier2').show();
        return false;
    }
    if (Validate3()){} else {
        $('#verifier3').show();
        return false;
    }
    if (Validate4()){} else {
        $('#verifier4').show();
        return false;
    }
    if (Validate5()){} else {
        $('#verifier5').show();
        return false;
    }
    //loadDoc();
    $('#clickme').click(loadDoc());
    return true;
}



function loadDoc() {
    var url = "https://ceclnx01.cec.miamioh.edu/~campbest/proxy.php?csurl=https://ceclnx01.cec.miamioh.edu/~castroa/cse383/homework08/form-ajax.php";
    //var url = "https://ceclnx01.cec.miamioh.edu/~castroa/cse383/homework08/form-ajax.php";

    //populates an object called person from the form so that it is easier to test with
    var person = {
        user: String($("#usr").val()),
        affiliation:$("#affiliation").val(),
        comment:$("#comment").val(),
        car:$("#car").val(),
        color:$("#color").val()
    }
    //testing alert to see if i am sending the right data
    //alert(JSON.stringify(person));

    var date = new Date();

    var timeValue;
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    if (hours > 0 && hours <= 12) {
        timeValue= "" + hours;
    } else if (hours > 12) {
        timeValue= "" + (hours - 12);
    } else if (hours == 0) {
        timeValue= "12";
    }
 
    timeValue += (minutes < 10) ? ":0" + minutes : ":" + minutes;  // get minutes
    timeValue += (seconds < 10) ? ":0" + seconds : ":" + seconds;  // get seconds
    timeValue += (hours >= 12) ? "pm" : "am";  // get AM/PM

    var correctedDate = date.getFullYear() + "-" +date.getMonth() + "-" + date.getDate() + " " + timeValue;

    $.ajax({          url: url,          type: "POST",          data: JSON.stringify(person),         dataType : "application/json",         success: function(text, response, jqXHR) { 

        document.getElementById("tableUser").innerHTML = (person.user).toUpperCase();
        document.getElementById("tableAffiliation").innerHTML = (person.affiliation).toUpperCase();
        document.getElementById("tableComment").innerHTML = (person.comment).toUpperCase();
        document.getElementById("tableCar").innerHTML = (person.car).toUpperCase();
        document.getElementById("tableColor").innerHTML = (person.color).toUpperCase();
        document.getElementById("tableTime").innerHTML = correctedDate;
            //here we do the css manipulation so that it shows properly.
            $("#table1").show();
            $("#form1").hide();             $('#ajax-data').html(text); 
            console.log("this is the result:", response);         },          error: function(jqXHR, exception, response) {
            alert("There was an error in the server try to submit the data again");
        },          complete: function( ) {              //alert( "The asynchronous task is done!" );          }     });  }

function toggle(){
    $("#form1").show();
    $("#table1").hide();
}